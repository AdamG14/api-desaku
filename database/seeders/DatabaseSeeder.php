<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Desa::factory()->count(1)->create();
        \App\Models\RukunWarga::factory()->count(1)->create();
        \App\Models\RukunTetangga::factory()->count(1)->create();
        \App\Models\User::factory()->count(4)->create();
        \App\Models\Notifikasi::factory()->count(1)->create();
        \App\Models\Surat::factory()->count(3)->create();
        \App\Models\Kegiatan::factory()->count(4)->create();
        \App\Models\Berita::factory()->count(4)->create();
        \App\Models\Post::factory()->count(4)->create();
        \App\Models\Comment::factory()->count(4)->create();
    }
}
