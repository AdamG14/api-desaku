<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;
    // RukunWarga::factory()->count(1)->create()
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
            static $role = 1;
            return [
                'nik' => Str::random(14),
                'name' => "Adam Ghirvan ".$role,
                'jenis_kelamin' => 1,
                'agama' => Str::random(5),
                'alamat' => Str::random(5),
                "tempat_lahir" => "Cilegon",
                "tanggal_lahir" => Carbon::now()->format('Y-m-d'),
                'no_telp' => Str::random(12),
                'kode_sms' => Str::random(8),
                'api_token' => Str::random(50),
                'status' => 1,
                'rt_id' => 1,
                'rw_id' => 1,
                'desa_id' => 1,
                'role' => $role++,
                'password' => bcrypt('140202'),
            ];      
    }
}
