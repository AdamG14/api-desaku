<?php

namespace Database\Factories;

use App\Models\Surat;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class SuratFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Surat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $status = 2;
        return [
            "no_surat" => rand(10,100)."/".rand(10,100),
            "name" => "Adam Ghirvan 1",
            "tempat_lahir" => "Cilegon",
            "tanggal_lahir" => Carbon::now()->format('Y-m-d'),
            "jenis_kelamin" => 1,
            "agama" => 1,
            "file"=> "dummy.pdf",
            "alamat" => "Sawojajar",
            "keperluan" => "Pengen nikah bangettt",
            "status" => $status++,
            "tingkat" => 1,
            "user_id" => 1,
            'rt_id' => 1,
            'rw_id' => 1,
            'desa_id' => 1,
        ];
    }
}
