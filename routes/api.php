<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\WargaController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\NotifikasiController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\SosmedController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('desaku')->group(function() {
    Route::post('register', [AuthController::class, 'register']);
    Route::get('select-list-desa', [AuthController::class, 'selectListDesa']);
    Route::get('select-list-rw/{id}', [AuthController::class, 'selectListRW']);
    Route::get('select-list-rt/{id}/{id2}', [AuthController::class, 'selectListRT']);
    Route::post('login', [AuthController::class, 'login']);
    Route::get('logout/{id}', [AuthController::class, 'logout']);
    Route::get('alert', [AuthController::class, 'alert'])->name('login');
    Route::post('check-sms', [AuthController::class, 'checkSMS']);

    Route::middleware('auth:api')->group(function() {
        Route::get('send-sms', [AuthController::class, 'sendSMS']);
        Route::prefix('warga')->group(function() {
            Route::post('create-surat', [WargaController::class, 'createSurat']);
            Route::get('list-pengajuan/', [AdminController::class, 'listPengajuanAdmin']);
        });
        Route::prefix('admin')->group(function() {
            Route::get('list-pengajuan/', [AdminController::class, 'listPengajuanAdmin']);
            Route::get('show-pengajuan/{id}', [AdminController::class, 'showPengajuanAdmin']);
            Route::get('reject-pengajuan/{id}', [AdminController::class, 'rejectPengajuan']);
            Route::get('accept-pengajuan/{id}', [AdminController::class, 'acceptPengajuan']);
            Route::get('select-list-rw', [AuthController::class, 'selectListRWAdmin']);
            Route::post('create-rt', [AuthController::class, 'createRT']);
            Route::get('list-rt', [AuthController::class, 'getRT']);
            Route::post('create-rw', [AuthController::class, 'createRW']);
            Route::get('list-rw', [AuthController::class, 'getRW']);
        });

        Route::prefix('notifikasi')->group(function() {
            Route::get('dashboard', [NotifikasiController::class, 'dashboardNotifikasi']);
            Route::get('detail', [NotifikasiController::class, 'detailNotifikasi']);
        });

        Route::get('profile-user', [AuthController::class, 'getUser']);
        Route::post('update-user', [AuthController::class, 'updateUser']);
        Route::get('info-user', [AuthController::class, 'infoUser']);

        Route::prefix('berita')->group(function() {
            Route::post('create', [BeritaController::class, 'createBerita']);
            Route::get('dashboard', [BeritaController::class, 'getBerita']);
            Route::get('lihat-semua', [BeritaController::class, 'allBerita']);
            Route::get('{id}', [BeritaController::class, 'showBerita']);
        });

        Route::prefix('kegiatan')->group(function() {
            Route::post('create', [KegiatanController::class, 'createKegiatan']);
            Route::get('dashboard', [KegiatanController::class, 'getKegiatan']);
            Route::get('lihat-semua', [KegiatanController::class, 'allKegiatan']);
            Route::get('{id}', [KegiatanController::class, 'showKegiatan']);
        });

        Route::prefix('sosmed')->group(function() {
            Route::post('create-post/{id}', [SosmedController::class, 'createPost']);
            Route::post('update/{id}', [SosmedController::class, 'updatePost']);
            Route::get('delete-post/{id}', [SosmedController::class, 'deletePost']);
            Route::post('create-comment/{id}', [SosmedController::class, 'createComment']);
            Route::get('get-comment/{id}', [SosmedController::class, 'getComment']);
            Route::get('delete-comment/{id}', [SosmedController::class, 'deleteComment']);
            Route::get('get-like/{id}', [SosmedController::class, 'addLike']);
            Route::get('remove-like/{id}', [SosmedController::class, 'removeLike']);
            Route::get('lihat-semua-usulan', [SosmedController::class, 'allPostUsulan']);
            Route::get('lihat-semua-laporan', [SosmedController::class, 'allPostLaporan']);
        });
    });
});