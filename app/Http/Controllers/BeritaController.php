<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RukunTetangga;
use App\Models\RukunWarga;
use App\Models\Notifikasi;
use App\Models\Berita;
use App\Models\Surat;
use Carbon\Carbon;
use App\Helper\Responses;
use Illuminate\Support\Facades\Auth;

class BeritaController extends Controller
{
    public function getBerita()
    {
        $helper = new responses();
        $berita = Berita::where('desa_id', Auth::user()->desa_id)->take(3)->get();        

        return $helper->responseMessageData('Berhasil ambil data', $berita);
    }

    public function allBerita()
    {
        $helper = new responses();
        $berita = Berita::where('desa_id', Auth::user()->desa_id)->get();        

        return $helper->responseMessageData('Berhasil ambil data', $berita);
    }

    public function showBerita($id)
    {
        $helper = new responses();
        $berita = Berita::find($id); 

        return $helper->responseMessageData('Berhasil ambil data', $berita);
    }

    public function createBerita(Request $req)
    {
        $helper = new responses();

        if(Auth::user()->role == "Admin") {
            $berita = new Berita; 
            $berita->judul = $req->judul;
            $berita->deskripsi = $req->deskripsi;
    
            $image = $req->file('image');
            $name = $image->getClientOriginalName();
            $image->move(public_path('images'), $name);
            $berita->img = $name;
    
            $berita->desa_id = Auth::user()->desa_id;
            $berita->save();
            return $helper->responseMessageData('Berhasil buat berita', $berita);
        } else {
            return $helper->responseError('Anda bukan admin!');
        }

    }
}
