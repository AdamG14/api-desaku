<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RukunTetangga;
use App\Models\RukunWarga;
use App\Models\Notifikasi;
use App\Models\Berita;
use App\Models\Surat;
use App\Models\Kegiatan;
use Carbon\Carbon;
use App\Helper\Responses;
use Illuminate\Support\Facades\Auth;

class KegiatanController extends Controller
{
    public function getKegiatan()
    {
        $helper = new responses();
        $kegiatan = Kegiatan::where('desa_id', Auth::user()->desa_id)->take(3)->get();        

        return $helper->responseMessageData('Berhasil ambil data', $kegiatan);
    }

    public function allKegiatan()
    {
        $helper = new responses();
        $kegiatan = Kegiatan::where('desa_id', Auth::user()->desa_id)->get();        

        return $helper->responseMessageData('Berhasil ambil data', $kegiatan);
    }

    public function showKegiatan($id)
    {
        $helper = new responses();
        $kegiatan = Kegiatan::find($id); 

        return $helper->responseMessageData('Berhasil ambil data', $kegiatan);
    }

    public function createKegiatan(Request $req)
    {
        $helper = new responses();

        if(Auth::user()->role == "Admin") {
            $kegiatan = new Kegiatan; 
            $kegiatan->tanggal = Carbon::parse($req->tanggal)->format("Y-m-d");
            $kegiatan->deskripsi = $req->deskripsi;
            $kegiatan->desa_id = Auth::user()->desa_id;
            $kegiatan->save();
            return $helper->responseMessageData('Berhasil buat kegiatan', $kegiatan);
        } else {
            return $helper->responseError('Anda bukan admin!');
        }

    }
}
