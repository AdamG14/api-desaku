<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RukunWarga;
use App\Models\RukunTetangga;
use App\Models\Desa;
use App\Helper\Responses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Nexmo\Laravel\Facade\Nexmo;

class AuthController extends Controller
{
    public function register(Request $request) {
        $helper = new Responses();

        $nik = $request->nik;
        $name = $request->name;
        $jenis_kelamin = $request->jenis_kelamin;
        $role = $request->role;
        $alamat = $request->alamat;
        $tempat_lahir = $request->tempat_lahir;
        $tanggal_lahir = Carbon::parse($request->tangal_lahir)->format('Y-m-d');
        $agama = $request->agama;
        $no_telp = $request->no_telp;
        $password = $request->password;
        $rt_id = $request->rt_id;
        $rw_id = $request->rw_id;
        $desa_id = $request->desa_id;

        if(User::where('nik', $nik)->count() < 1){
            
            $data = new User;
            $data->nik = $nik;
            $data->name = $name;
            $data->jenis_kelamin = $jenis_kelamin;
            $data->role = $role;
            $data->alamat = $alamat;
            $data->tempat_lahir = $tempat_lahir;
            $data->tanggal_lahir = $tanggal_lahir;
            $data->agama = $agama;
            $data->no_telp = $no_telp;
            $data->status = false;
            $data->password = bcrypt($password);
            $data->kode_sms = rand(10, 99).rand(10,99).rand(1,9);
            $data->api_token =Str::random(50);
            $data->rt_id = $rt_id;
            $data->rw_id = $rw_id;
            $data->desa_id =$desa_id;

            if($data->save()){
                Nexmo::message()->send([
                    'to'   => $data->no_telp,
                    'from' => 'Desaku',
                    'text' => 'Berikut kode verifikasi anda '.$data->kode_sms.' Silahkan membuka aplikasi Desaku lagi untuk verifikasi'
                ]);
                return $helper->responseMessage('Berhasil daftar, silahkan cek SMS');
            }
            else{
                return $helper->resposeError('Gagal daftar');
            }
        }
        else{
            return $helper->responseError('NIK sudah terdaftar');
        }
    }

    public function sendSMS()
    {
        Nexmo::message()->send([
            'to'   => Auth::user()->no_telp,
            'from' => 'Desaku',
            'text' => 'Berikut kode verifikasi anda '.Auth::user()->kode_sms.' Silahkan membuka aplikasi Desaku lagi untuk verifikasi'
        ]);

        return $helper->responseMessage('Berhasil kirim SMS');
    }

    public function checkSMS(Request $req) {
        $helper = new Responses();

        $kode_sms = $req->kode_sms;
        $user = User::where('kode_sms', $kode_sms)->first();
        if($user['kode_sms'] == $kode_sms){
            $user->status = true;
            $user->save();
            return $helper->responseMessageData('Berhasil Verifikasi', $user);
        }
        else{
            return $helper->responseError('Kode SMS Salah!');
        }
    }

    public function login(Request $request){
        $helper = new responses();

        $nik = $request->nik;
        $password = $request->password;


        $crd = ['nik' => $nik, 'password' => $password];

        if(Auth::attempt($crd)){
            $data = User::where('nik', $nik)->first();
            if($data->status == 1) {
                if(is_null($data->api_token) || $data->api_token == "") {
                    $data->api_token =Str::random(50);
                    $data->save();
                }
                return $helper->responseMessageData('Berhasil Login',$data);
            } else {
                return $helper->responseError('Anda belum mengaktivasi akun anda!!');
            }

        }
        else{
            return $helper->responseError('NIK atau Password anda salah!');
        }
    }

    public function logout($id){
        $helper = new responses();

        $data = User::find($id);
        $data->api_token = "";
        if($data->save()){
            return $helper->responseMessage('Berhasil Logout');
        }
        else{
            return $helper->responseError('Email atau Password anda salah!');
        }
    }

    public function selectListDesa(){
        $helper = new responses();
        $desa = Desa::select('id', 'nama')->get();        

        return $helper->responseMessageData('Berhasil ambil data', $desa);
    }

    public function selectListRW($id){
        $helper = new responses();
        $rw = RukunWarga::where('desa_id', $id)->select('id', 'nama')->get();        

        return $helper->responseMessageData('Berhasil ambil data', $rw);
    }

    public function selectListRT($id, $id2){
        $helper = new responses();
        $rt = RukunTetangga::where('desa_id', $id)->where('rw_id', $id2)->select('id', 'nama')->get();        

        return $helper->responseMessageData('Berhasil ambil data', $rt);
    }

    public function selectListRWAdmin(){
        $helper = new responses();
        $rw = RukunWarga::where('desa_id', Auth::user()->desa_id)->select('nama')->get();        

        return $helper->responseMessageData('Berhasil ambil data', $rw);
    }

    public function infoUser(){
        $helper = new responses();

        $userName = Auth::user()->name;
        $userRole = Auth::user()->role;
        $desa = Desa::find(Auth::user()->desa_id)->select('nama')->first();

        $data = [
            'nama' => $userName, 
            'role' => $userRole, 
            'desa' => $desa['nama']
        ];

        return $helper->responseMessageData('Berhasil ambil data', $data);
    }

    public function alert()
    {
        $helper = new responses();
        return $helper->responseError('Anda tidak terautentikasi');
    }

    public function getUser()
    {
        $helper = new responses();

        $data = Auth::user();
        if(!is_null($data)) {
            return $helper->responseMessageData('Berhasil ambil data', $data);
        }
    }

    public function updateUser(Request $request)
    {
        $helper = new responses();

        $nik = $request->nik;
        $name = $request->name;
        $jenis_kelamin = $request->jenis_kelamin;
        $alamat = $request->alamat;
        $tempat_lahir = $request->tempat_lahir;
        $tanggal_lahir = Carbon::parse($request->tanggal_lahir)->format('Y-m-d');
        $agama = $request->agama;
        $no_telp = $request->no_telp;
        

        $check = User::where('nik', $nik)->first();
        $stats = false;
        if($check == null) {
            $stats = true;
        } else if($check->id == Auth::user()->id) {
            $stats = true;
        }

        if($stats == true) {
            $data = User::find(Auth::user()->id);
            $data->nik = $nik == null ? $data->nik : $nik;
            $data->name = $name == null ? $data->name : $name;
            $data->jenis_kelamin = $jenis_kelamin == null ? $data->jenis_kelamin : $jenis_kelamin;
            $data->alamat = $alamat == null ? $data->alamat : $alamat;
            $data->tempat_lahir = $tempat_lahir == null ? $data->tempat_lahir : $tempat_lahir;
            $data->tanggal_lahir = $tanggal_lahir == null ? $data->tanggal_lahir : $tanggal_lahir;
            $data->agama = $agama == null ? $data->agama : $agama;
            $data->no_telp = $no_telp == null ? $data->no_telp : $no_telp;
            if($request->file('avatar') != null) {
                $image = $request->file('avatar');
            
                $name = $image->getClientOriginalName();
                $image->move(public_path('photo_profile'), $name);
                $data->avatar = $name;
            }
        } else {
            return $helper->responseError('NIK sudah dipakai orang lain');
        }
        if($request->password_baru != null) {
            if(Hash::check($request->password_lama, Auth::user()->password)) {
                $data->password = bcrypt($request->password_baru);
            } else {
                return $helper->responseError('Password lama anda salah!');
            }
        }

        if($data->save()) {
            return $helper->responseMessageData('Berhasil ubah data', $data);
        } else {
            return $helper->responseError('Gagal ubah data');
        }
    }

    public function createRT(Request $request)
    {
        $helper = new Responses();

        if(Auth::user()->role == "Admin") {
            $rt = new RukunTetangga;
            $rt->nama = $request->nama_rt; //nama RT
    
            $nik            = $request->nik;
            $jenis_kelamin  = "Laki-Laki";
            $role           = "RT";
            $alamat         = "";
            $tempat_lahir   = "";
            $tanggal_lahir  = Carbon::now()->format('Y-m-d');
            $agama          = "";
            $no_telp        = "";
            $password       = "Rahasia123!";
            $rw_id          = $request->rw_id;
            $desa_id        = Auth::user()->desa_id;
    
            if(User::where('nik', $nik)->count() < 1){
                $rt->rw_id = $rw_id;
                $rt->desa_id = $desa_id;
                $rt->save();
    
                $data = new User;
                $data->nik              = $nik;
                $data->name             = "";
                $data->jenis_kelamin    = $jenis_kelamin;
                $data->role             = $role;
                $data->alamat           = $alamat;
                $data->tempat_lahir     = $tempat_lahir;
                $data->tanggal_lahir    = $tanggal_lahir;
                $data->agama            = $agama;
                $data->no_telp          = $no_telp;
                $data->status           = true;
                $data->password         = bcrypt($password);
                $data->kode_sms         = rand(10, 99).rand(10,99).rand(1,9);
                $data->api_token        = Str::random(50);
                $data->rt_id            = $rt->id;
                $data->rw_id            = $rw_id;
                $data->desa_id          = $desa_id;
    
                if($data->save()){
                    return $helper->responseMessage('Berhasil membuat akun RT');
                }
                else{
                    return $helper->resposeError('Gagal daftar');
                }
            }
            else{
                return $helper->responseError('NIK sudah terdaftar');
            }
        } else {
            return $helper->responseError('Anda bukan ADMIN!');
        }

    }

    public function createRW(Request $request)
    {
        $helper = new Responses();

        if(Auth::user()->role == "Admin") {
            $rw = new RukunWarga;
            $rw->nama       = $request->nama_rw; //nama RW

            $nik            = $request->nik;
            $jenis_kelamin  = "Laki-Laki";
            $role           = "RW";
            $alamat         = "";
            $tempat_lahir   = "";
            $tanggal_lahir  = Carbon::now()->format('Y-m-d');
            $agama          = "";
            $no_telp        = "";
            $password       = "Rahasia123!";
            $desa_id        = Auth::user()->desa_id;

            if(User::where('nik', $nik)->count() < 1){
                $rw->desa_id = $desa_id;
                $rw->save();

                $data = new User;
                $data->nik              = $nik;
                $data->name             = "";
                $data->jenis_kelamin    = $jenis_kelamin;
                $data->role             = $role;
                $data->alamat           = $alamat;
                $data->tempat_lahir     = $tempat_lahir;
                $data->tanggal_lahir    = $tanggal_lahir;
                $data->agama            = $agama;
                $data->no_telp          = $no_telp;
                $data->status           = true;
                $data->password         = bcrypt($password);
                $data->kode_sms         = rand(10, 99).rand(10,99).rand(1,9);
                $data->api_token        =Str::random(50);
                $data->rw_id            = $rw->id;
                $data->desa_id          = $desa_id;

                if($data->save()){
                    return $helper->responseMessage('Berhasil membuat akun RW');
                }
                else{
                    return $helper->resposeError('Gagal daftar');
                }
            }
            else{
                return $helper->responseError('NIK sudah terdaftar');
            }
        } else {
            return $helper->responseError('Anda bukan ADMIN!');
        }
    }

    public function getRT()
    {
        $helper = new Responses();

        if(Auth::user()->role == "Admin") {
            $rt = User::where("desa_id", Auth::user()->desa_id)->where('role', 'RT')->get();
            $data = [];
            for($i = 0; $i < count($rt); $i++) {
                $each = [
                    "rt" => $rt[$i]->rt['nama'],
                    "rw" => $rt[$i]->rw['nama'],
                    "desa" => $rt[$i]->desa['nama'],
                    "nik" => $rt[$i]->nik,
                ];
                array_push($data, $each);
            }
            return $helper->responseMessageData('Berhasil mengambil data', $data);
        } else {
            return $helper->responseError('Anda bukan ADMIN!');
        }
    }

    public function getRW()
    {
        $helper = new Responses();

        if(Auth::user()->role == "Admin") {
            $rw = User::where("desa_id", Auth::user()->desa_id)->where('role', 'RW')->get();
            $data = [];
            for($i = 0; $i < count($rw); $i++) {
                $each = [
                    "rw" => $rw[$i]->rw['nama'],
                    "desa" => $rw[$i]->desa['nama'],
                    "nik" => $rw[$i]->nik
                ];
                array_push($data, $each);
            }
            return $helper->responseMessageData('Berhasil mengambil data', $data);
        } else {
            return $helper->responseError('Anda bukan ADMIN!');
        }
    }
}
