<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RukunTetangga;
use App\Models\RukunWarga;
use App\Models\Notifikasi;
use App\Models\Surat;
use Carbon\Carbon;
use App\Helper\Responses;
use Illuminate\Support\Facades\Auth;
use PDF;

class WargaController extends Controller
{
    public function createSurat(Request $req)
    {
        $helper = new Responses();

        $user = Auth::user();
        $rt_id = $user->rt_id;
        $rw_id = $user->rw_id;
        $count = Surat::where("user_id", $user->id)->count();
        $count++;

        $surat = new Surat;
        $surat->no_surat = $rt_id . "/" . $rw_id . "/" . $user->id . "/" . $count . "/" . Carbon::now()->year;
        $surat->name = $user->name;
        $surat->tempat_lahir = $user->tempat_lahir;
        $surat->tanggal_lahir = $user->tanggal_lahir;
        $surat->jenis_kelamin = $user->jenis_kelamin;
        $surat->agama = $user->agama;
        $surat->alamat = $user->alamat;
        $surat->keperluan = $req->keperluan;
        $surat->status = "Pending RT";
        $surat->tingkat = 1;
        $surat->user_id = $user->id;
        $surat->rt_id = $user->rt_id;
        $surat->rw_id = $user->rw_id;
        $surat->desa_id = $user->desa_id;
        $surat->file = "http://desaku.eventgap.id/file/".$surat->name."-".$count.".pdf";
        if($req->file('kk') != null) {
            $image = $req->file('kk');
        
            $name = $image->getClientOriginalName();
            $image->move(public_path('kk'), $name);
            $surat->kk = "http://desaku.eventgap.id/kk/".$name;
        } else {
            return $helper->resposeError('Gagal membuat surat');
        }
        if($req->file('ktp') != null) {
            $image = $req->file('ktp');
        
            $name = $image->getClientOriginalName();
            $image->move(public_path('ktp'), $name);
            $surat->ktp = "http://desaku.eventgap.id/ktp/".$name;
        } else {
            return $helper->resposeError('Gagal membuat surat');
        }
        if($req->file('akta') != null) {
            $image = $req->file('akta');
        
            $name = $image->getClientOriginalName();
            $image->move(public_path('akta'), $name);
            $surat->akta = "http://desaku.eventgap.id/akta/".$name;
        } else {
            return $helper->resposeError('Gagal membuat surat');
        }
        
        $ketuaRT = User::where('role', 'RT')->where('rt_id', $surat->rt_id)->first();
        $nomerRT = RukunTetangga::find($surat->rt_id)->nama;
        $nomerRW = RukunWarga::find($surat->rw_id)->nama;
        if($surat->save()) {
            $notif = new Notifikasi;
            $notif->judul = "Administrasi";
            $notif->deskripsi =  $user->name." membuat surat pengajuan ".$surat->keperluan. ", ketuk notifikasi ini untuk melihat detail";
            $notif->user_id = $ketuaRT->id;
            $notif->save();

            $pdf = PDF::loadview('surat_pdf',['surat'=>$surat, 'ketuaRT'=>$ketuaRT, 'nomerRT'=>$nomerRT, 'nomerRW'=>$nomerRW]);
            $pdf->save(public_path("file/".$surat->name."-".$count.".pdf"));
            return $helper->responseMessage('Berhasil membuat surat');
        } else {
            return $helper->resposeError('Gagal membuat surat');
        }
    }
}
