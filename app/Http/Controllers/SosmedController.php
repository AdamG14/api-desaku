<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RukunWarga;
use App\Models\RukunTetangga;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Like;
use App\Models\Desa;
use App\Helper\Responses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;

class SosmedController extends Controller
{
    public function createPost(Request $req, $id)
    {
        $helper = new responses();
        $post   = new Post;
        if($id == 1) {
            $post->caption = $req->caption;
            if($req->file('image') == null) {
                $post->image = "";
            } else {
                $image = $req->file('image');
            
                $name = $image->getClientOriginalName();
                $image->move(public_path('sosmed/usulan'), $name);
                $post->image = "http://desaku.eventgap.id/sosmed/usulan/".$name;
            }
            
            $post->tipe_post = 1;
            $post->user_id = Auth::user()->id;
            
            if($post->save()) {
                return $helper->responseMessageData('Berhasil membuat post', $post);
            } else {
                return $helper->responseError('Gagal buat post');
            }
        } else if($id == 2) {
            $post->caption = $req->caption;
            if($req->file('image') == null) {
                $post->image = "";
            } else {
                $image = $req->file('image');
            
                $name = $image->getClientOriginalName();
                $image->move(public_path('sosmed/laporan'), $name);
                $post->image = "http://desaku.eventgap.id/sosmed/laporan/" . $name;
            }
            $post->tipe_post = 2;
            $post->user_id = Auth::user()->id;
            
            if($post->save()) {
                return $helper->responseMessageData('Berhasil membuat post', $post);
            } else {
                return $helper->responseError('Gagal buat post');
            }
        }
    }

    public function allPostUsulan()
    {
        $helper = new responses();
        $data = [];
        $post = Post::where('tipe_post', 1)->get();
        foreach($post as $key => $p) {
            $comment = Comment::where('post_id', $p->id)->get();
            $allComment = [];
            foreach($comment as $key => $c) {
                $commentEach = [
                    'user'      => $c->user['name'],
                    'comment'   => $c->comment,
                    'image'     => $c->image,
                    'date'      => Carbon::parse($c->updated_at)->format('d F Y'),
                ];
                array_push($allComment, $commentEach);
            }
            $each = [
                'id'        => $p->id,
                'avatar'    => $p->user['avatar'],
                'caption'   => $p->caption,
                'image'     => $p->image,
                'date'      => Carbon::parse($p->updated_at)->format('d F Y'),
                'username'  => $p->user['name'],
                'totalLike' => Like::where('post_id', $p->id)->count(),
                'comment'   => $allComment,
                'likeCheck' => Like::where('post_id', $p->id)->where('user_id', Auth::user()->id)->first() == null ? false : true,
            ];
            
            array_push($data, $each);
        }

        return $helper->responseMessageData('Berhasil mengambil post', $data);
    }

    public function allPostLaporan()
    {
        $helper = new responses();
        $data = [];
        $post = Post::where('tipe_post', 2)->get();
        foreach($post as $key => $p) {
            $comment = Comment::where('post_id', $p->id)->get();
            $allComment = [];
            foreach($comment as $key => $c) {
                $commentEach = [
                    'user'      => $c->user['name'],
                    'comment'   => $c->comment,
                    'image'     => $c->image,
                    'date'      => Carbon::parse($c->updated_at)->format('d F Y'),
                ];
                array_push($allComment, $commentEach);
            }
            $each = [
                'id'        => $p->id,
                'avatar'    => $p->user['avatar'],
                'caption'   => $p->caption,
                'image'     => $p->image,
                'date'      => Carbon::parse($p->updated_at)->format('d F Y'),
                'username'  => $p->user['name'],
                'totalLike' => Like::where('post_id', $p->id)->count(),
                'comment'   => $allComment,
                'likeCheck' => Like::where('post_id', $p->id)->where('user_id', Auth::user()->id)->first() == null ? false : true,
            ];
            
            array_push($data, $each);
        }

        return $helper->responseMessageData('Berhasil mengambil post', $data);
    }

    public function updatePost($id)
    {
        $helper = new responses();
        $post   = Post::find($id);

        $post->caption = $req->caption;
        if($req->file('image') != null) {
            $image = $req->file('image');
        
            $name = $image->getClientOriginalName();
            $image->move(public_path('sosmed\usulan'), $name);
            $post->image = $name;
        }
        
        if($post->save()) {
            return $helper->responseMessageData('Berhasil mengubah post', $post);
        } else {
            return $helper->responseError('Gagal buat post');
        }
    }

    public function deletePost($id)
    {
        $helper = new responses();
        $post   = Post::find($id);
        
        if($post->delete()) {
            return $helper->responseMessage('Berhasil menghapus post');
        } else {
            return $helper->responseError('Gagal mengubah post');
        }
    }

    public function createComment($id)
    {
        $helper = new responses();
        $data   = new Comment;

        $data->comment = $req->comment;
        $data->user_id = Auth::user()->id;
        $data->post_id = $id;
        if($req->file('image') != null) {
            $image = $req->file('image');
        
            $name = $image->getClientOriginalName();
            $image->move(public_path('sosmed\comment'), $name);
            $data->image = $name;
        }
        
        if($data->save()) {
            return $helper->responseMessageData('Berhasil membuat comment', $data);
        } else {
            return $helper->responseError('Gagal membuat comment');
        }
    }

    public function getComment($id)
    {
        $helper = new responses();
        $data = [];
        $comment = Comment::where('post_id', $id)->get();
        $allComment = [];
        foreach($comment as $key => $c) {
            $commentEach = [
                'user'      => $c->user['name'],
                'comment'   => $c->comment,
                'image'     => $c->image,
                'date'      => Carbon::parse($c->updated_at)->format('d F Y'),
            ];
            array_push($allComment, $commentEach);
        }
        
        return $helper->responseMessageData('Berhasil mengambil comment', $allComment);
    }

    public function deleteComment($id)
    {
        $helper = new responses();
        $data   = Comment::find($id);
        
        if($data->delete()) {
            return $helper->responseMessage('Berhasil menghapus comment');
        } else {
            return $helper->responseError('Gagal hapus comment');
        }
    }

    public function addLike($id)
    {
        $helper = new responses();
        $data   = new Like;

        $data->user_id = Auth::user()->id;
        $data->post_id = $id;
        
        if($data->save()) {
            return $helper->responseMessage('Berhasil membuat like');
        } else {
            return $helper->responseError('Gagal membuat like');
        }
    }

    public function removeLike($id)
    {
        $helper = new responses();
        $data   = Like::where('user_id', Auth::user()->id)->where('post_id', $id)->first();
        
        if($data->delete()) {
            return $helper->responseMessage('Berhasil menghapus like');
        } else {
            return $helper->responseError('Gagal menghapus like');
        }
    }
}
