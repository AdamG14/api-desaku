<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\Responses;
use App\Models\Notifikasi;

class NotifikasiController extends Controller
{
    public function dashboardNotifikasi()
    {
        try{
            $helper = new Responses();
            $notif = Notifikasi::where('user_id', Auth::user()->id)->latest('created_at')->take(3)->get();

            return $helper->responseMessage($notif);

        }catch(Exception $e) {
            return $helper->responseError('Gagal mengambil data, '.$e);
        }
    }

    public function detailNotifikasi()
    {
        try{
            $helper = new Responses();
            $notif = Notifikasi::where('user_id', Auth::user()->id)->latest('created_at')->get();

            return $helper->responseMessage($notif);

        }catch(Exception $e) {
            return $helper->responseError('Gagal mengambil data, '.$e);
        }
    }
}
