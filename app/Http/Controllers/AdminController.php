<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Surat;
use App\Models\Notifikasi;
use App\Helper\Responses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function listPengajuanAdmin()
    {
        try{
            $helper = new Responses();
        
            $user = Auth::user();
            if($user->role == "Admin") {
                $surat = Surat::where('desa_id', $user->desa_id)->where('status', 'Pending Admin')->orWhere('status', 'Disetujui')->get();
    
                return $helper->responseMessage($surat);
            } else if ($user->role == "RT"){
                $surat = Surat::where('desa_id', $user->desa_id)->where('rw_id',$user->rw_id)->where('rt_id', $user->rt_id)->where('status', 'Pending RT')->get();

                return $helper->responseMessage($surat);
            } else if ($user->role == "RW"){
                $surat = Surat::where('desa_id', $user->desa_id)->where('rw_id', $user->rw_id)->where('status', 'Pending RW')->get();

                return $helper->responseMessage($surat);
            } else {
                return $helper->responseError('ANDA BUKAN ADMIN');
            }

        }catch(Exception $e) {
            return $helper->responseError('Gagal mengambil data, '.$e);
        }

    }

    public function showPengajuanAdmin($id)
    {
        try{
            $helper = new Responses();
            $data = [];
            $user = Auth::user();
            if($user->role == "Admin" || $user->role == "RT" || $user->role == "RW") {
                $surat = Surat::select('id', 'no_surat', 'name', 'tempat_lahir', 'tanggal_lahir'
                , 'jenis_kelamin', 'agama', 'alamat', 'keperluan', 'file','kk','ktp', 'akta', 'status', 'tingkat')->find($id);
                $buatAmbil = Surat::find($id);
                $data = [
                    "detail" => $surat,
                    "rt" => $buatAmbil->rt['nama'],
                    "rw" => $buatAmbil->rw['nama'],
                    "desa" => $buatAmbil->desa['nama'],
                ];

                return $helper->responseMessage($data);
            } else {
                return $helper->responseError('ANDA BUKAN ADMIN');
            }

        }catch(Exception $e) {
            return $helper->responseError('Gagal mengambil data, '.$e);
        }

    }

    public function rejectPengajuan($id)
    {
        try{
            $helper = new Responses();
        
            if(Auth::user()->role == "Admin") {
                $surat = Surat::find($id);
                $surat->status = "Ditolak";

                $notif = new Notifikasi;
                $notif->judul = "Administrasi";
                $notif->deskripsi = "Surat ".$surat->keperluan." anda ditolak";
                $notif->user_id = $surat->user_id;

                $notif->save();
                $surat->save();
    
                return $helper->responseMessage($surat);
            } else if (Auth::user()->role == "RT"){
                $surat = Surat::find($id);
                $surat->status = "Ditolak";

                $notif = new Notifikasi;
                $notif->judul = "Administrasi";
                $notif->deskripsi = "Surat ".$surat->keperluan." anda ditolak";
                $notif->user_id = $surat->user_id;

                $notif->save();
                $surat->save();

                return $helper->responseMessage($surat);
            } else if (Auth::user()->role == "RW"){
                $surat = Surat::find($id);
                $surat->status = "Ditolak";

                $notif = new Notifikasi;
                $notif->judul = "Administrasi";
                $notif->deskripsi = "Surat ".$surat->keperluan." anda ditolak";
                $notif->user_id = $surat->user_id;
                
                $notif->save();
                $surat->save();

                return $helper->responseMessage($surat);
            } else {
                return $helper->responseError('ANDA BUKAN PENGURUS');
            }

        }catch(Exception $e) {
            return $helper->responseError('Gagal mengubah data, '.$e);
        }

    }

    public function acceptPengajuan($id)
    {
        try{
            $helper = new Responses();
        
            if(Auth::user()->role == "Admin") {
                $surat = Surat::find($id);
                $surat->status = "Disetujui";

                $notif = new Notifikasi;
                $notif->judul = "Administrasi";
                $notif->deskripsi = "Surat ".$surat->keperluan." anda diterima mohon menuju kantor desa mengambil surat tersebut";
                $notif->user_id = $surat->user_id;

                $notif->save();
                $surat->save();
    
                return $helper->responseMessage($surat);
            } else if (Auth::user()->role == "RT"){
                $surat = Surat::find($id);
                $surat->status = "Pending RW";

                $notif = new Notifikasi;
                $notif->judul = "Administrasi";
                $notif->deskripsi = "Surat ".$surat->keperluan." anda diterima mohon tunggu persetujuan RW";
                $notif->user_id = $surat->user_id;

                $notif2 = new Notifikasi;
                $notif2->judul = "Administrasi";
                $notif2->deskripsi =  $surat->user['name']." membuat surat pengajuan ".$surat->keperluan. ", ketuk notifikasi ini untuk melihat detail";
                $notif2->user_id = User::where('role', 'RW')->where('rw_id', $surat->rw_id)->first()->id;

                $notif2->save();
                $notif->save();
                $surat->save();

                return $helper->responseMessage($surat);
            } else if (Auth::user()->role == "RW"){
                $surat = Surat::find($id);
                $surat->status = "Pending Admin";

                $notif = new Notifikasi;
                $notif->judul = "Administrasi";
                $notif->deskripsi = "Surat ".$surat->keperluan." anda diterima mohon tunggu persetujuan Administrasi Desa";
                $notif->user_id = $surat->user_id;

                
                $notif2 = new Notifikasi;
                $notif2->judul = "Administrasi";
                $notif2->deskripsi =  $surat->user['name']." membuat surat pengajuan ".$surat->keperluan. ", ketuk notifikasi ini untuk melihat detail";
                $notif2->user_id = User::where('role', 'Admin')->where('desa_id', $surat->desa_id)->first()->id;

                $notif2->save();
                $notif->save();
                $surat->save();
                return $helper->responseMessage($surat);
            } else {
                return $helper->responseError('ANDA BUKAN PENGURUS');
            }

        }catch(Exception $e) {
            return $helper->responseError('Gagal mengubah data, '.$e);
        }

    }
}
