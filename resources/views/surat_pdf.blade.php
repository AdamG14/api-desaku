<html>
 <head>
  <title> Cara Membikin Surat </title>
 </head>

        <body bgcolor="white">
            <div>
            <font face="Arial" color="black"> <p align="center"  style="font-weight: bold;"> RUKUN TETANGGA {{$surat->rt['nama']}}/{{$surat->rw['nama']}} PERUMAHAN DUMMY ALAM DUMMY 
            <br>{{$surat->desa['nama']}}, KECAMATAN DUMMY 
            <br>KABUPATEN DUMMY</p></font>
            <!-- <font face="Arial"> <p align="center"  style="font-weight: bold;"> SEKOLAH MENENGAH KEJURUAN NEGERI 1 CIREBON </p></font> -->
            <!-- <font face="Arial" size="3"> <p align="center"> JL. Perjuangan By Pass Sunyaragi Telp.(0231) 123456 Cirebon 45141 </p></font> -->
            <hr>
            
            <font face="Arial" size="6"> <p align="center"> <u> <b> SURAT KETERANGAN</b></u></font><br>
            <font face="Arial"  size="4"> Nomer: 1/1/1/2020 </p></font>

            <p>Yang bertanda tangan di bawah ini Ketua RT 01 RW 01 Desa Asik Menerangkan Bahwa :</p>
            <table>
                <tr><td>1. Nama Lengkap</td><td>:</td><td>{{ $surat->name }}</td></tr>
                <tr><td>2. Tempat/Tgl Lahir</td><td>:</td><td>{{ $surat->tempat_lahir }}/{{$surat->tanggal_lahir}}</td></tr>
                <tr><td>3. Jenis Kelamin</td><td>:</td><td>{{ $surat->jenis_kelamin }}</td></tr>
                <tr><td>4. Agama</td><td>:</td><td>{{ $surat->agama }}</td></tr>
                <tr><td>5. Alamat</td><td>:</td><td>{{ $surat->alamat }}</td></tr>
                <tr><td>6. Keperluan</td><td>:</td><td>{{ $surat->keperluan }}</td></tr>
            </table>
            <p>Demikian surat keterangan ini dibuat untuk dapat dipergunakan sesuai dengan keperluannya.</p>
            <br>
            <br>
            <p style="text-align: right;">Ketua {{$nomerRT}}/{{$nomerRW}}</p>
            <br><br><br>
            <p style="text-align: right;">{{$ketuaRT->name}}</p>
        </div>

        </body>
</html> 